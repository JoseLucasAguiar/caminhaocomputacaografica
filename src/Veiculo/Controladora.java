package Veiculo;


import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;


public class Controladora 
        implements GLEventListener, KeyListener {

    private final GLU glu = new GLU();
    private final GLUT glut = new GLUT();
    private final Caminhao caminhao = new Caminhao();
    
    private final double x = Dados.x;
    private final double _CacambaRotation = Dados._CacambaRotation;
    private final double _CaminhaoRotation = Dados._CaminhaoRotation;
    
    private final boolean _SubirCacamba = Dados._SubirCacamba;
    private final boolean _DescerCacamba = Dados._DescerCacamba;
    private final boolean _MoverFrente = Dados._MoverFrente;
    private final boolean _MoverTras = Dados._MoverTras;
    private final boolean _Rotacionar = Dados._Rotacionar;
    
    public Controladora()
    {
        GLJPanel canvas = new GLJPanel();
        canvas.addGLEventListener(this);
        
        JFrame frame = new JFrame("Caminhãozinho");
        frame.setSize(500, 500);
        frame.getContentPane().add(canvas);
        frame.setVisible(true);
        frame.addKeyListener(this);
        
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Thread(new Runnable() {
                    public void run() {
                        System.exit(0);
                    }
                }).start();
            }
        });
    }
    
    @Override
    public void init(GLAutoDrawable glAuto) {
        Animator a = new Animator(glAuto);
        a.start();
        GL gl = glAuto.getGL();
        gl.glClearColor(1f, 1f, 1f, 1f);
      
        gl.glEnable(GL.GL_DEPTH_TEST);
    }
    
    
    public void desenha(GL2 gl){
        caminhao.desenhaCaminhao(gl, glut);
        caminhao.acoes();
    }
    
    @Override
    public void display(GLAutoDrawable glAuto) {

        GL2 gl = glAuto.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT |
                   GL.GL_DEPTH_BUFFER_BIT
        );
        
        gl.glLoadIdentity();
        gl.glTranslated(0,0,-10);
        
        desenha(gl);   
    }
    
    @Override
    public void keyPressed(KeyEvent e){
        int event = e.getKeyCode();
        
        switch (event) {
            case KeyEvent.VK_UP:
                caminhao.movimentaFrente();
            break;
            case KeyEvent.VK_DOWN:
                caminhao.movimentaTras();
            break;
            case KeyEvent.VK_W:
                caminhao.sobeCacamba();
            break;
            case KeyEvent.VK_S:
                caminhao.desceCacamba();
                break;
            case KeyEvent.VK_RIGHT:
                caminhao.rotacionaDireita();
                break;
            case KeyEvent.VK_LEFT:
                caminhao.rotacionaEsquerda();
                break;
            default:
                break;
        }
   
    }
    
    public void reshape(GLAutoDrawable gLAutoDrawable, int x, int y, int w, int h) {
  
        GL2 gl = gLAutoDrawable.getGL().getGL2(); 
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(60,1,1,300);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslated(0,0,-10);
    }

    @Override
    public void dispose(GLAutoDrawable glad) {}
    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    public void keyReleased(KeyEvent e) {}
}
