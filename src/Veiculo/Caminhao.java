/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veiculo;

import javax.media.opengl.GL2;
import com.jogamp.opengl.util.gl2.GLUT;
/**
 *
 * @author Jose Lucas
 */
public class Caminhao {
    
    private double x = Dados.x;
    private double _CacambaRotation = Dados._CacambaRotation;
    private double _CaminhaoRotation = Dados._CaminhaoRotation;
    
    private boolean _SubirCacamba = Dados._SubirCacamba;
    private boolean _DescerCacamba = Dados._DescerCacamba;
    private boolean _MoverFrente = Dados._MoverFrente;
    private boolean _MoverTras = Dados._MoverTras;
    private boolean _Rotacionar = Dados._Rotacionar;
    private double g = Dados._RotacionaCaminhao;
    private boolean _RotacionaEsquerda;
    private boolean _RotacionaDireita;
    
    public void desenhaCaminhao(GL2 gl, GLUT glut){
         gl.glTranslated(x, 0, 0);
        
         
         gl.glRotated(g, 0, 1,0);
        gl.glPushMatrix();
        
            gl.glColor3d(0, 0, 0);
            
            // CARRETA
            gl.glPushMatrix();
                gl.glRotated(_CacambaRotation, 0, 0, 1);
                gl.glTranslated(1, 0.5, 0);
                gl.glScaled(2, 1, 2);
                glut.glutSolidCube(1);    
            gl.glPopMatrix();

            // MINHÃO
            gl.glPushMatrix();
                gl.glColor3d(0.2, 0.2, 0.2);
                gl.glTranslated(1, -0.25, 0);
                gl.glScaled(2, 0.5, 2);
                glut.glutSolidCube(1);
            gl.glPopMatrix();

            // CABINE BASE
            gl.glPushMatrix();
            gl.glColor3d(0.2, 0.2, 0.2);
                gl.glTranslated(2, -0.25, 0);
                gl.glScaled(2, 0.5, 2);
                glut.glutSolidCube(1);
            gl.glPopMatrix();
            
            // CABINE MOTORISTA
            gl.glPushMatrix();
                gl.glColor3d(1, 0, 0);
                gl.glTranslated(2.5, 0.75, 0);
                gl.glScaled(1, 1.5, 2);
                glut.glutSolidCube(1);
            gl.glPopMatrix();
            
            // CABINE FRONTAL - FUCINHO 
            gl.glPushMatrix();
                gl.glColor3d(0, 1, 0);
                gl.glTranslated(2.8, 0.5, 0);
                gl.glScaled(0.45, 0.75, 1);
                glut.glutSolidCube(1);
            gl.glPopMatrix();
            
            // CABINE FRONTAL - VIDRO DA FRENTE
            gl.glPushMatrix();
                gl.glColor3d(0, 0, 1);
                gl.glTranslated(2.8, 1, 0);
                gl.glScaled(0.5, 0.9, 1.7);
                glut.glutSolidCube(1);
            gl.glPopMatrix();
            
            // RODA TRASEIRA - ESQUERDA
            gl.glPushMatrix();
                gl.glColor3d(0, 0, 0);
                gl.glTranslated(0.5, -0.5, -1);
                gl.glScaled(0.2, 0.2, 0.2);
                glut.glutSolidSphere(1, 20, 20);
            gl.glPopMatrix();

            // RODA TRASEIRA - DIREITA
            gl.glPushMatrix();
                gl.glColor3d(0, 0, 0);
                gl.glTranslated(0.5, -0.5, 1);
                gl.glScaled(0.2, 0.2, 0.2);
                glut.glutSolidSphere(1, 20, 20);
            gl.glPopMatrix();
            
            // RODA FRONTAL - DIREITA
            gl.glPushMatrix();
                gl.glColor3d(0, 0, 0);
                gl.glTranslated(2.5, -0.5, 1);
                gl.glScaled(0.2, 0.2, 0.2);
                glut.glutSolidSphere(1, 20, 20);
            gl.glPopMatrix();
            
            // RODA FRONTAL - ESQUERDA
            gl.glPushMatrix();
                gl.glColor3d(0, 0, 0);
                gl.glTranslated(2.5, -0.5, -1);
                gl.glScaled(0.2, 0.2, 0.2);
                glut.glutSolidSphere(1, 20, 20);
            gl.glPopMatrix();
            
        gl.glPopMatrix();
    }
    
    public void acoes(){
        if(_MoverFrente)
          x += 0.002;
        else if(_MoverTras)
            x -= 0.002;
        
        if(_RotacionaDireita){
            g += 0.2;
        }else if(_RotacionaEsquerda)
            g -= 0.2;
        
        if(_SubirCacamba)
            _CacambaRotation += 0.02;
        else if(_DescerCacamba)
            _CacambaRotation -= 0.02;
        
//        if(_Rotacionar)
//            gl.glRotated(0, 0, 1, 0);
//        else
//            _CaminhaoRotation = 0;
        
        if(_CacambaRotation >= 45)
            _SubirCacamba = false;
        else if(_CacambaRotation <= 0)
            _DescerCacamba = false;
    }
    
    public void movimentaFrente(){
        if(_MoverTras){
            _MoverFrente = false;
            _MoverTras = false;
        }else{
            _MoverFrente = true;
            _MoverTras = false;
        }
    }   
    
    public void movimentaTras(){
        if(_MoverFrente){
            _MoverTras = false;
            _MoverFrente = false;
        }else{
            _MoverTras = true;
            _MoverFrente = false;
        }   
    }
    
    public void sobeCacamba(){
        if(_DescerCacamba){
            _SubirCacamba = false;
            _DescerCacamba = false;
        }else{
            _SubirCacamba = true;
            _DescerCacamba = false;
        }   
    }
    
    public void desceCacamba(){
        if(_SubirCacamba){
            _SubirCacamba = false;
            _DescerCacamba = false;
        }else{
            _SubirCacamba = false;
            _DescerCacamba = true;
        }   
    }

    
    
    public void rotacionaEsquerda() {
        if(_RotacionaDireita){
            _RotacionaDireita = false;
            _RotacionaEsquerda = false;
        }else{
            _RotacionaEsquerda = true;
        }
    }
    
    public void rotacionaDireita() {
        if(_RotacionaEsquerda){
            _RotacionaDireita = false;
            _RotacionaEsquerda = false;
        }else{
            _RotacionaDireita = true;
        }
    }
}
